/*
 * mmhelper/main.c - Mastermind helper, main routine
 * Copyright (C) 2020  Octavio Alvarez Piza
 * SPDX-License-Identifier: AGPL-3.0-only
 *
 * This program is distributed under the Affero GPL 3.0 license.
 * Licensing terms are included in the LICENSE file.
 * 
 * This program is distributed WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mmlogic.h"

void print_table4(enum code_selection option_set[8][8][8][8][8], unsigned int colors) {
    for (unsigned int i = 0; i < colors; i++)
        for (unsigned int j = 0; j < colors; j++)
            for (unsigned int k = 0; k < colors; k++) {
                if (k % 3 == 0)
                    printf("\n");
                for (unsigned int l = 0; l < colors; l++)
                    if (option_set[i][j][k][l][0] == POSSIBLE)
                        printf("%u%u%u%u ", i + 1, j + 1, k + 1, l + 1);
                    else
                        printf("---- ");
            }
}

void print_table5(enum code_selection option_set[8][8][8][8][8], unsigned int colors) {
    for (unsigned int i = 0; i < colors; i++)
        for (unsigned int j = 0; j < colors; j++)
            for (unsigned int k = 0; k < colors; k++)
                for (unsigned int l = 0; l < colors; l++) {
                    if (l % 2 == 0)
                        printf("\n");
                    for (unsigned int m = 0; m < colors; m++) {
                        if (option_set[i][j][k][l][m] == POSSIBLE)
                            printf("%u%u%u%u%u ", i + 1, j + 1, k + 1, l + 1, m + 1);
                        else
                            printf("----- ");
                    }
            }
}

int main(int const argc, char const * const argv[]) {

    enum code_selection option_set[8][8][8][8][8] = {POSSIBLE};

    int paramstart = 1;
    unsigned int spaces = 4;
    unsigned int colors = 6;
    void (*print_table)(enum code_selection o[8][8][8][8][8], unsigned int) = print_table4;

    if (argc == 1) {
        fprintf(stderr, "usage: mmhelper [--58] code1 result1 ...\n");
        exit(EXIT_FAILURE);
    }

    if (strncmp(argv[1], "--58", 5) == 0) {
        spaces = 5;
        colors = 8;
        paramstart = 2;
        print_table = print_table5;
    }

    if (strncmp(argv[1], "-h", 3) == 0) {
        printf("usage: mmhelper [--58] code1 result1 ...\n");
        exit(EXIT_SUCCESS);
    }

    if (((argc - paramstart) & 1) == 1) {
        fprintf(stderr, "usage: mmhelper [--58] code1 result1 ...\n");
        exit(EXIT_FAILURE);
    }

    // Run through each pair of arguments, taking them as option + result.
    // Discard impossible guesses.
    for (int a = paramstart; a < argc; a += 2) {

        enum code_selection result_option_set[8][8][8][8][8] = {POSSIBLE};
        code guess;
        int guessresult;

        // Parse the next guess into internal format from the command line.
        for (unsigned int i = 0; i < spaces; i++) {
            guess[i] = argv[a][i] - 0x30 - 1;
        }

        guessresult = atoi(argv[a + 1]);

        eliminate_options(option_set, spaces, guess, guessresult, result_option_set);
        
        memcpy(option_set, result_option_set,
            8 * 8 * 8 * 8 * 8 * sizeof(enum code_selection));
    }

    // Print resulting option table.    
    print_table(option_set, colors);
    return 0;
}
