mmhelper
========

This program is a tool to aid in solving Mastermind puzzles in its original
format (6 colors, 4 spaces).

Usage
=====

``mmhelper [--58] guess result [guess result] ...``

mmhelper takes a list of (guess,result) pairs and prints back the set of
possible secrets remaining.

Guesses are expressed as a sequence of four numbers from 1 to 6.

Results are expressed as a 2-digit number:
 * The first digit expresses the amount of colored/black key pegs, this is,
   the amount of guess pegs that turned out to be correct and placed in the
   correct position.
 * The second digit expresses the amount of white key pegs, this is, the
   amount of guess pegs that exist in the secret code but are not placed
   in the correct position.

For example, if the first guess was 1122 and the codemaker gave back black,
black, white, this can be expressed by calling mmhelper like this:

``mmhelper 1122 21``

If a second attempt is made, 2125, and the result is three black pegs, run
mmhelper like this:

``mmhelper 1122 21 2125 30``

mmhelper will show that the secret code is either 2123, 2124 or 2126.


Options
=======

 * Option ``--58`` will use a 5-space, 8-color game mode.


Compilation
===========

Type "make". You will get a binary called mmhelper.

