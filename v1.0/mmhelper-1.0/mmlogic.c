/*
 * mmhelper/mmlogic.c - Mastermind helper, logic routines
 * Copyright (C) 2020  Octavio Alvarez Piza
 * SPDX-License-Identifier: AGPL-3.0-only
 *
 * This program is distributed under the Affero GPL 3.0 license.
 * Licensing terms are included in the LICENSE file.
 * 
 * This program is distributed WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <string.h>

#include "mmlogic.h"

unsigned int check_guess(code const _guess, unsigned int spaces, code const _secret) {

    unsigned int guess[5] = {0};
    unsigned int secret[5] = {0};
    
    unsigned int blacks = 0;
    unsigned int whites = 0;
    
    // TBH, start_white will always equal to blacks,
    // but programmed separately for to improve legibility.
    unsigned int start_whites = 0;
    
    memcpy(guess, _guess, spaces * sizeof(unsigned int));
    memcpy(secret, _secret, spaces * sizeof(unsigned int));

    for (unsigned int i = 0; i < spaces; i++) {
        if (guess[i] == secret[i]) {
            guess[i] = guess[start_whites];
            secret[i] = secret[start_whites];
            blacks += 1;
            start_whites += 1;
        }
    }

    for (unsigned int i = start_whites; i < spaces; i++) {
        for (unsigned int j = start_whites; j < spaces; j++) {
            if (guess[i] == secret[j]) {
                secret[j] = secret[start_whites];
                whites += 1;
                start_whites += 1;
                break;
            }
        }
    }
    
    return 10 * blacks + whites;
}

void eliminate_options(
    // Input
    enum code_selection option_set[8][8][8][8][8],
    unsigned int spaces,
    code const guess,
    unsigned int const result,
    // Output
    enum code_selection returned_option_set[8][8][8][8][8]
) {
    
    memcpy(returned_option_set, option_set,
        8 * 8 * 8 * 8 * 8 * sizeof(enum code_selection));
    
    for (unsigned int m = 0; m < 8 && (spaces > 4 || m == 0); m++)
    for (unsigned int i = 0; i < 8; i++)
    for (unsigned int j = 0; j < 8; j++)
    for (unsigned int k = 0; k < 8; k++)
    for (unsigned int l = 0; l < 8; l++) {
        if (option_set[i][j][k][l][m] == DISCARDED)
            continue;
        code option = {i, j, k, l, m};
        unsigned int r = check_guess(option, spaces, guess);
        if (r != result) {
            returned_option_set[i][j][k][l][m] = DISCARDED;
        }
    }
}
