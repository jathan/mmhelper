Format: 3.0 (quilt)
Source: mmhelper
Binary: mmhelper
Architecture: any
Version: 1.0-1
Maintainer: Jonathan Bustillos <jathan@debian.org>
Homepage: https://gitlab.com/alvarezp2000/mmhelper
Standards-Version: 3.9.8
Vcs-Git: https://gitlab.com/alvarezp2000/mmhelper
Build-Depends: debhelper (>= 9)
Package-List:
 mmhelper deb games optional arch=any
Checksums-Sha1:
 82e0bc7c25803107db79c1a1c3d2a905f1654394 16719 mmhelper_1.0.orig.tar.gz
 443f38e41fcce2bd15fff43ac21c52303edb326d 2144 mmhelper_1.0-1.debian.tar.xz
Checksums-Sha256:
 443f0f8612e4626e6a5e8c8d8da4ffbad2c169e981af8ebb5f3b2a45eb965940 16719 mmhelper_1.0.orig.tar.gz
 117b193c57b1e9da76e3189c3af2d2a13a3ce53d82e374ccc150054bf7435a33 2144 mmhelper_1.0-1.debian.tar.xz
Files:
 1e432150455420b07b92a125aaa823df 16719 mmhelper_1.0.orig.tar.gz
 7af78d6ac2bdb9dd7b735ce9f931cf4e 2144 mmhelper_1.0-1.debian.tar.xz
